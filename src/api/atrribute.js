import request from '@/utils/request'

export function fetchAtrributeList(params) {
  return request({
    url: '/product/category/attribute?id=' + params, // 类型id, 通过类型id返回类型下的所有属性
    method: 'get'
  })
}

// 根据attrId(属性id)获取属性信息
export function fetchAttributeInfo(id) {
  return request({
    url: '/product/category/attribute/' + id,
    method: 'get'
  })
}

// 修改属性
export function updateAttr(data) {
  return request({
    url: '/product/category/attribute/update',
    method: 'put',
    data: data
  })
}
// 添加商品属性
export function addAttr(data) {
  return request({
    url: '/product/category/attribute',
    method: 'post',
    data: data
  })
}

export function deleteAttr(id) {
  return request({
    url: '/product/category/attribute/' + id,
    method: 'delete'
  })
}
