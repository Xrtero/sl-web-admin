import request from '@/utils/request'

// 获取所有权限信息
export function fetchPermissionList(params) {
  return request({
    url: '/sys/admin/permission',
    method: 'get',
    params: params
  })
}
export function updatePermission(data) {
  return request({
    url: '/sys/admin/permission',
    method: 'put',
    data: data
  })
}

export function addPermission(data) {
  return request({
    url: '/sys/admin/permission',
    method: 'post',
    data: data
  })
}

export function deletePermission(params) {
  return request({
    url: '/sys/admin/permission',
    method: 'delete',
    params: params
  })
}
