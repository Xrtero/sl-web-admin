import request from '@/utils/request'

export function fetchReturnList(params) {
  return request({
    url: '/order/return',
    method: 'get',
    params: params
  })
}

export function updateReturnOrder(id, params) {
  return request({
    url: '/order/return/' + id,
    method: 'post',
    params: params
  })
}
