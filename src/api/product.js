import request from '@/utils/request'

export function fetchProductList(params) {
  return request({
    url: '/product',
    method: 'get',
    params: params
  })
}
export function updateProduct(id, params) {
  return request({
    url: '/product' + id,
    method: 'put',
    params: params
  })
}

export function deleteProduct(id) {
  return request({
    url: '/product/' + id,
    method: 'delete'
  })
}

export function fetchProductLog(id) {
  return request({
    url: '/product/log/' + id,
    method: 'get'
  })
}

export function searchProduct(params) {
  return request({
    url: '/product/search',
    method: 'GET',
    params: params
  })
}

export function fetchProductInfo(id) {
  return request({
    url: '/product/' + id,
    method: 'get'
  })
}
