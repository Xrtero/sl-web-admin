import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/admin/login',
    method: 'post',
    data: data
  })
}
export function register(data) {
  return request({
    url: '/sys/admin/register',
    method: 'post',
    data: data
  })
}
export function getInfo(token) {
  return request({
    url: '/sys/admin/info',
    method: 'get',
    params: { token }
  })
}

export function logout() {
  return request({
    url: '/sys/admin/logout',
    method: 'get'
  })
}

export function fetchUserList(params) {
  return request({
    url: '/sys/admin',
    method: 'get',
    params: params
  })
}

export function updateUserInfo(params) {
  return request({
    url: '/admin',
    method: 'put',
    params: params
  })
}

export function addUserInfo(params) {
  return request({
    url: '/admin',
    method: 'POST',
    params: params
  })
}

export function deleteUserInfo(params) {
  return request({
    url: '/admin',
    method: 'DELETE',
    params: params
  })
}
