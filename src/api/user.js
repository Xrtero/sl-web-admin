import request from '@/utils/request'

export function fetchUserList(params) {
  return request({
    url: '/user',
    method: 'get',
    params: params
  })
}

export function registerUser(params) {
  return request({
    url: '/user',
    method: 'POST',
    params: params
  })
}