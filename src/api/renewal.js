import request from '@/utils/request'

export function fetchRenewalList() {
  return request({
    url: '/order/renewal',
    method: 'get'
  })
}

export function fetchRenewalOrderInfo(id) {
  return request({
    url: '/order/renewal/' + id,
    method: 'get'
  })
}

export function deleteRenewalOrder(id) {
  return request({
    url: '/order/renewal/' + id,
    method: 'delete'
  })
}

export function updateRenewalOrder(id, params) {
  return request({
    url: '/order/renewal/' + id,
    method: 'put',
    params: params
  })
}

