import request from '@/utils/request'

export function fetchRoleNameList() {
  return request({
    url: '/sys/admin/role/name',
    method: 'get'
  })
}

export function fetchRoleList(params) {
  return request({
    url: '/sys/admin/role',
    method: 'get',
    params: params
  })
}

export function updateRoleInfo(params) {
  return request({
    url: '/sys/admin/role',
    method: 'PUT',
    params: params
  })
}

export function addRoleInfo(params) {
  return request({
    url: '/sys/admin/role',
    method: 'POST',
    params: params
  })
}

export function deleteRoleInfo(params) {
  return request({
    url: '/sys/admin/role',
    method: 'DELETE',
    params: params
  })
}

export function updateUserRoleInfo(id, data) {
  return request({
    url: '/sys/admin/role/' + id,
    method: 'PUT',
    params: data
  })
}
