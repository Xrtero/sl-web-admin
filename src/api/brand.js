import request from '@/utils/request'

// 获取品牌列表
export function fetchBrandList(params) {
  return request({
    url: '/product/brand',
    method: 'get',
    params: params
  })
}

// 获取所有品牌名称
export function fetchBrandName() {
  return request({
    url: '/product/brand/name',
    method: 'get'
  })
}

// 按品牌id来获取品牌名称
export function fetchBrandById(id) {
  return request({
    url: '/product/brand/' + id,
    method: 'get'
  })
}

// 按id删除品牌
export function deleteBrand(id) {
  return request({
    url: '/product/brand/' + id,
    method: 'delete'
  })
}

// 添加品牌
export function addBrand(params) {
  return request({
    url: '/product/brand',
    method: 'post',
    params: params
  })
}

// 修改品牌
export function updateBrand(id, params) {
  return request({
    url: '/product/brand' + id,
    method: 'put',
    params: params
  })
}
