import request from '@/utils/request'

export function fetchCategoryList(params) {
  return request({
    url: '/product/category',
    method: 'get',
    params: params
  })
}
export function fetchCategoryName() {
  return request({
    url: '/product/category/name',
    methodo: 'get'
  })
}
export function createCategory(params) {
  return request({
    url: '/product/category',
    method: 'post',
    body: params
  })
}

export function createSubCategory(params) {
  return request({
    url: '/product/category/sub',
    method: 'POST',
    body: params
  })
}

export function updateCategory(id, params) {
  return request({
    url: '/product/category/' + id,
    method: 'put',
    body: params
  })
}

export function updataSubCategory(id, params) {
  return request({
    url: '/product/category/sub/' + id,
    method: 'put',
    body: params
  })
}

export function deleteCategory(id) {
  return request({
    url: '/product/category/' + id,
    method: 'DELETE'
  })
}
