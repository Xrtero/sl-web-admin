import request from '@/utils/request'

export function fetchOrderList(params) {
  return request({
    url: '/order',
    method: 'get',
    params: params
  })
}

export function fetchOrderDetail(id) {
  return request({
    url: '/order/' + id,
    method: 'get'
  })
}

export function deleteOrder(id) {
  return request({
    url: '/order/' + id,
    method: 'delete'
  })
}

export function updateOrder(id, params) {
  return request({
    url: '/order',
    method: 'put',
    params: params
  })
}
