import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },
  {
    path: '/',
    component: Layout,
    redirect: '/home',
    children: [
      {
        path: 'home',
        component: () => import('@/views/home/index'),
        name: 'Home',
        meta: { title: '主页', icon: 'home', arrfix: 'true' }
      }
    ]
  },
  {
    path: '/404',
    component: () => import('@/views/error-page/404'),
    hidden: true
  }
]

export const asyncRoutes = [

  {
    path: '/product',
    component: Layout,
    redirect: '/product/list',
    meta: {
      title: '商品管理',
      icon: 'product'
    },
    children: [
      {
        path: 'list',
        component: () => import('@/views/product/list/index'),
        name: 'productList',
        meta: { title: '商品列表', icon: 'list' }
      },
      {
        path: 'productInfo/:id(\\d+)',
        component: () => import('@/views/product/list/productInfo'),
        name: 'productInfo'
      },
      {
        path: 'category',
        component: () => import('@/views/product/category/index'),
        name: 'Category',
        meta: { title: '商品类别', icon: 'category' }
      },
      {
        path: 'attribute',
        component: () => import('@/views/product/attribute/index'),
        name: 'Attribute',
        meta: { title: '商品属性', icon: 'attribute' }
      },
      {
        path: 'brand',
        component: () => import('@/views/product/brand/index'),
        name: 'brand',
        meta: { title: '品牌', icon: 'brand' }
      },
      {
        path: 'brand/add',
        component: () => import('@/views/product/brand/addBrand'),
        name: 'addBrand',
        meta: { title: '添加品牌', noCache: true },
        hidden: true
      },
      {
        path: 'brand/edit/:id(\\d+)',
        component: () => import('@/views/product/brand/editBrand'),
        name: 'editBrand',
        meta: { title: '编辑品牌', noCache: true },
        hidden: true
      },
      {
        path: 'attribute/:cid(\\d+)',
        component: () => import('@/views/product/attribute/viewAttributeList'),
        name: 'AttributeList',
        meta: { title: '属性列表', noCache: true },
        hidden: true
      },
      {
        path: 'attribute/edit/:id(\\d+)',
        component: () => import('@/views/product/attribute/updateAttribute'),
        name: 'updateAttribute',
        meta: { title: '编辑属性', noCache: true },
        hidden: true
      },
      {
        path: 'attribute/add/:id(\\d+)',
        component: () => import('@/views/product/attribute/addAttribute'),
        name: 'addAttribute',
        meta: { title: '添加属性', noCache: true },
        hidden: true
      }
    ]
  },

  {
    path: '/order',
    component: Layout,
    redirec: '/order/list',
    meta: { title: '订单管理', icon: 'order' },
    children: [
      {
        path: 'list',
        name: 'Order List',
        component: () => import('@/views/order/list/index'),
        meta: { title: '订单列表', icon: 'list' }
      },
      {
        path: ':id(\\d+)',
        name: 'order',
        component: () => import('@/views/order/list/orderInfo'),
        meta: { title: '订单' },
        hidden: true
      },
      {
        path: 'relet',
        name: 'order relet',
        component: () => import('@/views/order/renewal/index'),
        meta: { title: '续租列表', icon: 'renewal' }
      },
      {
        path: 'return',
        name: 'return',
        component: () => import('@/views/order/return/index'),
        meta: { title: '退货申请处理', icon: 'return' }
      }
    ]
  },
  {
    path: '/user',
    component: Layout,
    redirec: '/user/list',
    meta: { title: '用户管理', icon: 'user' },
    children: [
      {
        path: 'list',
        name: 'userList',
        component: () => import('@/views/user/list/index'),
        meta: { title: '用户列表', icon: 'list' }
      }/* ,
      {
        path: 'chat',
        name: 'Chat',
        component: () => import('@/views/user/chat/index'),
        meta: { title: '聊天记录', icon: 'chat' }
      } */
    ]
  },
  {
    path: '/admin',
    component: Layout,
    redirec: '/admin/list',
    meta: { title: '管理员', icon: 'admin' },
    children: [
      {
        path: 'list',
        name: 'adminList',
        component: () => import('@/views/admin/list/index'),
        meta: { title: '管理员列表', icon: 'list' }
      },
      {
        path: 'role',
        name: 'Role',
        component: () => import('@/views/admin/role/index'),
        meta: { title: '角色', icon: 'role' }
      },
      {
        path: 'rolePower/:id(\\d+)',
        name: 'RolePower',
        component: () => import('@/views/admin/role/rolepower'),
        meta: { title: '角色权限' },
        hidden: true
      },
      {
        path: 'permission',
        name: 'permission',
        component: () => import('@/views/admin/permission/index'),
        meta: { title: '权限管理', icon: 'power' }
      }
    ]
  },
  /*   {
    path: '/monitor',
    component: Layout,
    redirec: '/monitor/page',
    meta: { title: '监控', icon: 'monitor' },
    children: [
      {
        path: 'page',
        name: 'Page',
        alwaysShow: true,
        component: () => import('@/views/monitor/system-info/index'),
        meta: { title: '使用情况', icon: 'system-info' }
      }
    ]
  }, */

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }
]

const createRouter = () => new Router({
  // mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
